package com.unityindiversity.worldwar2warship;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

public class WebSiteScenes extends AppCompatActivity {

    private WebView myWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_site_scenes);

        Bundle bundle = getIntent().getExtras();
        String name = bundle.getString("name");
        String from = bundle.getString("from");
        String image = bundle.getString("image");
        final String web_site = bundle.getString("webSite");

        TextView warship_name = findViewById(R.id.warshipName);
        TextView warship_from = findViewById(R.id.warshipSpecs);
        ImageView image_warship = findViewById(R.id.img_item_photo);

        Glide.with(this)
                .load(image)
                .apply(new RequestOptions().override(150, 150))
                .into(image_warship);
        warship_name.setText(name);
        warship_from.setText(from);


        //load the Website that we want to call
        myWebView = findViewById(R.id.webSearch);
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setSupportMultipleWindows(false);

        myWebView.loadUrl(web_site);
    }
}
