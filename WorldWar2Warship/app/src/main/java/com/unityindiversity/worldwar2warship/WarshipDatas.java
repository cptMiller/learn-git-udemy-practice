package com.unityindiversity.worldwar2warship;

import java.util.ArrayList;

public class WarshipDatas {
    public static String[][] datas = new String[][]{
            {"KRI Gadjah Mada","a.k.a HNLMS Tjerk Hiddes a.k.a HMS Nonpareil merupakan N-class destroyer yang dibuat di Britania Raya pada tanggal 22 Mei 1940 oleh William Denny and Brothers, Dumbarton. Yang di beli oleh Indonesia pada tanggal 1 Maret 1951","https://upload.wikimedia.org/wikipedia/commons/9/98/Aankomst_Tjerk_Hiddes_in_Rotterdam%2C_Bestanddeelnr_903-6348.jpg","https://en.wikipedia.org/wiki/HNLMS_Tjerk_Hiddes_(G16)"},
            {"KRI Irian","a.k.a RNS Sverdlov (Project 68-bis) merupakan light cruiser yang dibuat di Russia pada tanggal 19 Oktober 1949 dan di beli oleh Indonesia pada tahun 1963","https://upload.wikimedia.org/wikipedia/id/9/9d/Irian.jpg","https://id.wikipedia.org/wiki/KRI_Irian_(201)"},
            {"USS Iowa","USS Iowa (BB-61) adalah kapal utama dari enam kapal di kelasnya dan dibuat Angkatan Laut Amerika Serikat dari tahun 1939 hingga 1940. Kapal ini digunakan pertama kali pasca Perang Dunia II. Diluncurkan: 27 Agustus 1942","https://upload.wikimedia.org/wikipedia/commons/e/ea/BB61_USS_Iowa_BB61_broadside_USN.jpg","https://id.wikipedia.org/wiki/USS_Iowa_(BB-61)"},
            {"USS Missouri","USS Missouri (BB-63) (Mighty Mo atau Big Mo) adalah sebuah kapal tempur kelas Iowa milik Angkatan Laut Amerika Serikat sekaligus kapal Angkatan Laut Amerika Serikat ketiga yang dinamai negara bagian Angkatan Laut Amerika Serikat (Missouri). Diluncurkan: 29 Januari 1944","https://upload.wikimedia.org/wikipedia/commons/e/e8/Missouri_post_refit.JPG","https://id.wikipedia.org/wiki/USS_Missouri_(BB-63)"},
            {"IJN Yamato","Yamato adalah kapal tempur Angkatan Laut Kekaisaran Jepang dalam Perang Dunia II, sekaligus kapal utama dalam Armada Gabungan. Diluncurkan: 8 Agustus 1940","https://upload.wikimedia.org/wikipedia/commons/1/12/Yamatotrials.jpg","https://id.wikipedia.org/wiki/Kapal_tempur_Jepang_Yamato"},
            {"IJN Musashi","Musashi adalah kapal kedua dari kelas yamato Perang Dunia II milik Angkatan Laut Kekaisaran Jepang. Diluncurkan: 1 November 1940","https://upload.wikimedia.org/wikipedia/commons/0/0c/Japanese_battleship_Musashi_cropped.jpg","https://id.wikipedia.org/wiki/Kapal_tempur_Jepang_Musashi"},
            {"KMS Bismark","Bismarck adalah kapal tempur terbesar yang pernah dibuat Jerman pada masa Perang Dunia II. Diluncurkan: 14 Februari 1939","https://upload.wikimedia.org/wikipedia/commons/f/fe/Bundesarchiv_Bild_193-04-1-26%2C_Schlachtschiff_Bismarck.jpg","https://id.wikipedia.org/wiki/Kapal_tempur_Jerman_Bismarck"},
            {"KMS Tirpitz","Tirpitz adalah kapal perang milik Jerman pada masa Perang Dunia II. Kapal ini merupakan kapal kedua dari kelas Bismarck yang diluncurkan pada tahun 1939.","https://upload.wikimedia.org/wikipedia/commons/7/79/Tirpitz-2.jpg","https://en.wikipedia.org/wiki/German_battleship_Tirpitz"},
            {"HMS Queen Elizabeth","merupakan dreadnought kakak dari Warspite yang dibuat di Britania Raya pada tanggal 21 Oktober 1912","https://upload.wikimedia.org/wikipedia/commons/9/9e/British_Warships_of_the_Second_World_War_A9256.jpg","https://en.wikipedia.org/wiki/HMS_Queen_Elizabeth_(1913)"},
            {"HMS Warspite","merupakan dreadnought class Queen Elizabeth yang dibuat di Britania Raya pada tanggal 31 Oktober 1912","https://upload.wikimedia.org/wikipedia/commons/c/c0/HMS_Warspite%2C_Indian_Ocean_1942.jpg","https://en.wikipedia.org/wiki/HMS_Warspite_(03)"},
            {"Gangut Class Battleship","Kapal tempur kelas Gangut (disebut juga Kapal tempur kelas Sevastopol), adalah kelas kapal tempur dreadnought pertama Angkatan Laut Kekaisaran Rusia yang dibuat sebelum Perang Dunia I. Kapal dalam kelas ini memiliki sejarah yang alot, termasuk konflik dengan beberapa perusahaan Inggris, spesifikasi yang berubah-ubah, kompetisi desain kapal internasional, dan berbagai protes dari banyak pihak.","https://upload.wikimedia.org/wikipedia/commons/d/dc/Gangut_battleship.jpg","https://id.wikipedia.org/wiki/Kapal_tempur_kelas_Gangut"},
            {"USS Woshington","USS Washington (BB-56) was the second and final member of the North Carolina class of fast battleships, the first vessel of the type built for the United States Navy.","https://upload.wikimedia.org/wikipedia/commons/2/22/USS_Washington_%28BB-56%29_in_Puget_Sound%2C_10_September_1945.jpg","https://en.wikipedia.org/wiki/USS_Washington_(BB-56)"},
            {"USS South Dakota","USS South Dakota (BB-57) was the lead vessel of the four South Dakota-class fast battleships built for the United States Navy in the 1930s.","https://upload.wikimedia.org/wikipedia/commons/a/ad/USS_South_Dakota_%28BB-57%29_anchored_in_Hvalfj%C3%B6r%C3%B0ur%2C_Iceland%2C_on_24_June_1943_%28NH_97265%29.jpg","https://en.wikipedia.org/wiki/USS_South_Dakota_(BB-57)"},
            {"USS Enterprise","USS Enterprise (CV-6) was the seventh U.S. Navy vessel named Enterprise. Colloquially called The Big E, she was the sixth aircraft carrier of the United States Navy.","https://upload.wikimedia.org/wikipedia/commons/8/87/USS_Enterprise_%28CV-6%29_in_Puget_Sound%2C_September_1945.jpg","https://en.wikipedia.org/wiki/USS_Enterprise_(CV-6)"},
            {"USS Colorado","USS Colorado (BB-45) was a battleship of the United States Navy that was in service from 1923 to 1947. She was the lead ship of the Colorado class of battleships.","https://upload.wikimedia.org/wikipedia/commons/2/20/USS_Colorado_%28BB-45%29_New_York_1932.jpg","https://en.wikipedia.org/wiki/USS_Colorado_(BB-45)"},
            {"USS Maryland","USS Maryland (BB-46), also known as Old Mary or Fighting Mary to her crewmates, was a Colorado-class battleship. She was the third ship of the United States Navy to be named in honor of the seventh state.","https://upload.wikimedia.org/wikipedia/commons/e/e2/USS_Maryland_%28BB-46%29_underway_in_1935.jpg","https://en.wikipedia.org/wiki/USS_Maryland_(BB-46)"},
            {"USS West Virginia","USS West Virginia (BB-48) was the fourth dreadnought battleship of the Colorado class, though because Washington was cancelled, she was the third and final member of the class to be completed.","https://upload.wikimedia.org/wikipedia/commons/7/79/USS_West_Virginia_BB-48.jpg","https://en.wikipedia.org/wiki/USS_West_Virginia_(BB-48)"},
    };

    public static ArrayList<WarShip> getWarshipListData() {
        ArrayList<WarShip> warshipslist = new ArrayList<>();
        for (String[] aData : datas) {
            WarShip ws = new WarShip();
            ws.setName(aData[0]);
            ws.setFrom(aData[1]);
            ws.setPhoto(aData[2]);
            ws.setWarshiplink(aData[3]);

            warshipslist.add(ws);
        }
        return warshipslist;
    }
}
