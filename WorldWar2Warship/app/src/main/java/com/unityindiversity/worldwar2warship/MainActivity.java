package com.unityindiversity.worldwar2warship;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;



import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private RecyclerView rvWarship;
    private ArrayList<WarShip> listDatas = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rvWarship = findViewById(R.id.rv_warship);
        rvWarship.setHasFixedSize(true);

        listDatas.addAll(WarshipDatas.getWarshipListData());
        showRecyclerList();

    }


   private void showRecyclerList() {

       rvWarship.setLayoutManager(new LinearLayoutManager(this));
       ListWarshipAdapter listWarshipAdapter = new ListWarshipAdapter(listDatas);
       rvWarship.setAdapter(listWarshipAdapter);
       listWarshipAdapter.setOnItemClickCallback(new ListWarshipAdapter.OnItemClickCallback() {
           @Override
           public void onItemClicked(WarShip data) {

               Bundle bundle = new Bundle();
               bundle.putString("name", data.getName());
               bundle.putString("from",data.getFrom());
               bundle.putString("image", data.getPhoto());
               bundle.putString("webSite", data.getWarshiplink());
               Intent intent = new Intent(MainActivity.this, WebSiteScenes.class);
               intent.putExtras(bundle);
               startActivity(intent);
           }
       });

   }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.about_us :
                Intent moveIntent = new Intent(MainActivity.this, AboutUs.class);
                startActivity(moveIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
