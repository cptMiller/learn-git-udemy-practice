package com.unityindiversity.worldwar2warship;

public class WarShip {
    private String name, from, photo;
    private String Warshiplink;

    public String getWarshiplink() {
        return Warshiplink;
    }

    public void setWarshiplink(String warshiplink) {
        Warshiplink = warshiplink;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
